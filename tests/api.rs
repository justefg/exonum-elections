extern crate exonum;
extern crate exonum_elections as elections;
extern crate exonum_testkit;
#[macro_use]
extern crate serde_json;

use exonum::{
    api::node::public::explorer::TransactionQuery,
    crypto::{self, CryptoHash, Hash, PublicKey},
};
use exonum_testkit::{ApiKind, TestKit, TestKitApi, TestKitBuilder};

use elections::{
    api::{CandidateInfo, CandidateQuery}, transactions::{CreateVote, CreateCandidate}, 
    structs::Candidate, structs::Vote, structs::ElectionResult, structs::EmptyArg, Service,
    structs::Citizen,
};

use constants::{ALICE_NAME, BOB_NAME};
mod constants;

mod tests {
    use super::*;
    #[test]
    fn test_create_candidate() {
        let (mut testkit, api) = create_testkit();

        let tx = api.create_candidate(ALICE_NAME);
        testkit.create_block();
        api.assert_tx_status(tx.hash(), &json!({ "type": "success" }));

        let candidate = api.get_candidate(*tx.pub_key()).unwrap();
        assert_eq!(candidate.pub_key(), tx.pub_key());
        assert_eq!(candidate.name(), tx.name());
        assert_eq!(candidate.votes_count(), 0);
    }

    /// Check that a citizen can vote
    #[test]
    fn test_create_vote() {
        let (mut testkit, api) = create_testkit();
        let tx_alice = api.create_candidate(ALICE_NAME);

        testkit.create_block();

        let (pubkey, key) = crypto::gen_keypair();
        let tx = CreateVote::new(
            &pubkey,
            BOB_NAME,
            tx_alice.pub_key(),
            0,
            &key
        );
        api.create_vote(&tx);
        testkit.create_block();

        let candidate = api.get_candidate(*tx_alice.pub_key()).unwrap();
        assert_eq!(candidate.votes_count(), 1);
    }

    /// Check that double-voting is not allowed
    #[test]
    fn test_create_vote_double() {
        let (mut testkit, api) = create_testkit();
        let tx_alice = api.create_candidate(ALICE_NAME);
        let (pubkey, key) = crypto::gen_keypair();
        testkit.create_block();
        let tx1 = CreateVote::new(
            &pubkey,
            BOB_NAME,
            tx_alice.pub_key(),
            0,
            &key
        );
        let tx2 = CreateVote::new(
            &pubkey,
            BOB_NAME,
            tx_alice.pub_key(),
            1,
            &key
        );
        api.create_vote(&tx1);
        api.create_vote(&tx2);
        testkit.create_block_with_tx_hashes(&[tx1.hash(), tx2.hash()]);
        api.assert_tx_status(
            tx2.hash(),
            &json!({ "type": "error", "code": 1, "description": "Citizen has already voted" })
        );
    }

    /// Check that we cannot vote for a non-existing person
    #[test]
    fn test_create_vote_non_existing_candidate() {
        let (mut testkit, api) = create_testkit();
        let tx_alice = api.create_candidate(ALICE_NAME);
        let (pubkey, key) = crypto::gen_keypair();
        // Do not commit !
        // testkit.create_block();
        let tx = CreateVote::new(
            &pubkey,
            BOB_NAME,
            tx_alice.pub_key(),
            0,
            &key
        );
        api.create_vote(&tx);
        testkit.create_block_with_tx_hashes(&[tx.hash()]);
        api.assert_tx_status(
            tx.hash(),
            &json!({ "type": "error", "code": 2, "description": "Candidate doesn't exist" })
        );
    }

    #[test]
    fn test_run_elections() {
        let (mut testkit, api) = create_testkit();
        let tx_alice = api.create_candidate(ALICE_NAME);
        let tx_bob = api.create_candidate(BOB_NAME);

        testkit.create_block();

        let (c_pubkey, c_key) = crypto::gen_keypair();
        let (d_pubkey, d_key) = crypto::gen_keypair();
        let (e_pubkey, e_key) = crypto::gen_keypair();

        let tx1 = CreateVote::new(
            &c_pubkey,
            "Alex",
            tx_alice.pub_key(),
            0,
            &c_key
        );
        let tx2 = CreateVote::new(
            &d_pubkey,
            "Beth",
            tx_bob.pub_key(),
            0,
            &d_key
        );

        let tx3 = CreateVote::new(
            &e_pubkey,
            "Anthony",
            tx_alice.pub_key(),
            0,
            &e_key
        );

        api.create_vote(&tx1);
        api.create_vote(&tx2);
        api.create_vote(&tx3);
        testkit.create_block_with_tx_hashes(&[tx1.hash(), tx2.hash(), tx3.hash()]);
        let result = api.get_result().result;
        assert_eq!(result.len(), 2);
        assert_eq!(result[0].votes_count, 2);
        assert_eq!(result[0].candidate, Citizen::new(*tx_alice.pub_key(), ALICE_NAME.to_string()));

        assert_eq!(result[1].votes_count, 1);
        assert_eq!(result[1].candidate, Citizen::new(*tx_bob.pub_key(), BOB_NAME.to_string()));

    }
}

struct ElectionApi {
    pub inner: TestKitApi,
}

impl ElectionApi {
    fn create_candidate(&self, name: &str) -> CreateCandidate {
        let (pubkey, key) = crypto::gen_keypair();
        let tx = CreateCandidate::new(&pubkey, name, &key);

        let tx_info: serde_json::Value = self.inner
            .public(ApiKind::Service("elections"))
            .query(&tx)
            .post("v1/candidate/new")
            .unwrap();
        assert_eq!(tx_info, json!({ "tx_hash": tx.hash() }));
        tx
    }

    fn get_candidate(&self, pub_key: PublicKey) -> Option<Candidate> {
        let candidate_info = self.inner
            .public(ApiKind::Service("elections"))
            .query(&CandidateQuery { pub_key })
            .get::<CandidateInfo>("v1/candidate/info")
            .unwrap();

        let to_candidate = candidate_info.candidate_proof.to_candidate.check().unwrap();
        to_candidate
            .all_entries()
            .iter()
            .find(|(ref k, _)| **k == pub_key)
            .and_then(|tuple| tuple.1)
            .cloned()
    }

    fn create_vote(&self, tx: &CreateVote) {
        let tx_info: serde_json::Value = self.inner
            .public(ApiKind::Service("elections"))
            .query(&tx)
            .post("v1/vote")
            .unwrap();
        assert_eq!(tx_info, json!({ "tx_hash": tx.hash() }));
    }
    fn get_result(&self) -> ElectionResult {
        self.inner
            .public(ApiKind::Service("elections"))
            .query(&EmptyArg::new(1))
            .get::<ElectionResult>("v1/result")
            .unwrap()

    }
    fn assert_tx_status(&self, tx_hash: Hash, expected_status: &serde_json::Value) {
        let info = self.inner
            .public(ApiKind::Explorer)
            .query(&TransactionQuery::new(tx_hash))
            .get("v1/transactions")
            .unwrap();

        if let serde_json::Value::Object(mut info) = info {
            let tx_status = info.remove("status").unwrap();
            assert_eq!(tx_status, *expected_status);
        } else {
            panic!("Invalid transaction info format, object expected");
        }
    }
}

fn create_testkit() -> (TestKit, ElectionApi) {
    let testkit = TestKitBuilder::validator().with_service(Service).create();
    let api = ElectionApi {
        inner: testkit.api(),
    };
    (testkit, api)
}
