#![allow(bare_trait_objects)]

use exonum::{
    blockchain::{ExecutionResult, Transaction}, crypto::{CryptoHash, PublicKey},
    messages::Message, storage::Fork,
};

use schema::Schema;
use structs::Vote;
use ELECTIONS_SERVICE_ID;

use errors::Error;

transactions! {
    pub ElectionTransactions {
        const SERVICE_ID = ELECTIONS_SERVICE_ID;
        struct CreateVote {
            from: &PublicKey,
            name: &str,
            to: &PublicKey,
            seed: u64,
        }
        struct CreateCandidate {
            pub_key: &PublicKey,
            name: &str,
        }
    }
}

impl Transaction for CreateVote {
    fn verify(&self) -> bool {
        self.verify_signature(self.from())
    }

    fn execute(&self, fork: &mut Fork) -> ExecutionResult {
        let mut schema = Schema::new(fork);
        let from = self.from();
        let to = self.to();
        let hash = self.hash();
        if schema.vote(from).is_some() {
            Err(Error::CitizenAlreadyVoted)?
        }
        let candidate = schema.candidate(to).ok_or(Error::CandidateNotFound)?;
        schema.add_vote(Vote::new(from, self.name(), to), candidate, &hash);
        Ok(())
    }
}

impl Transaction for CreateCandidate {
    fn verify(&self) -> bool {
        self.verify_signature(self.pub_key())
    }

    fn execute(&self, fork: &mut Fork) -> ExecutionResult {
        let mut schema = Schema::new(fork);
        let pub_key = self.pub_key();
        let hash = self.hash();

        if schema.candidate(pub_key).is_none() {
            let name = self.name();
            schema.create_candidate(pub_key, name, &hash);
            Ok(())
        } else {
            Err(Error::CandidateAlreadyExists)?
        }
    }
}
