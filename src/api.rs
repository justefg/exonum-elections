use exonum::{
    api::{self, ServiceApiBuilder, ServiceApiState},
    blockchain::{self, BlockProof, Transaction, TransactionSet}, crypto::{Hash, PublicKey},
    helpers::Height, node::TransactionSend, storage::{ListProof, MapProof},  
};

use transactions::ElectionTransactions;
use structs::Candidate;
use structs::EmptyArg;
use structs::{Citizen, Citizens};
use structs::{ElectionResult, ElectionResultItem, ElectionResultItems};
use {Schema, ELECTIONS_SERVICE_ID};

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct CandidateQuery {
    pub pub_key: PublicKey,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TransactionResponse {
    pub tx_hash: Hash,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CandidateProof {
    pub to_table: MapProof<Hash, Hash>,
    pub to_candidate: MapProof<PublicKey, Candidate>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CandidateHistory {
    pub proof: ListProof<Hash>,
    pub transactions: Vec<ElectionTransactions>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CandidateInfo {
    pub block_proof: BlockProof,
    pub candidate_proof: CandidateProof,
    pub candidate_history: CandidateHistory,
}

#[derive(Debug, Clone, Copy)]
pub struct PublicApi;

use errors::Error;

impl PublicApi {
    pub fn result(state: &ServiceApiState, _query: EmptyArg) -> api::Result<ElectionResult>{
        let snapshot = state.snapshot();
        let general_schema = blockchain::Schema::new(&snapshot);
        let elections_schema = Schema::new(&snapshot);
        let candidates = elections_schema.candidates();
        let mut result : ElectionResultItems = Vec::new();
        for candidate in candidates.values() {

            let history = elections_schema.candidate_history(candidate.pub_key());

            let mut transactions: Vec<ElectionTransactions> = history
                .iter()
                .map(|record| general_schema.transactions().get(&record).unwrap())
                .map(|raw| ElectionTransactions::tx_from_raw(raw).unwrap())
                .collect::<Vec<_>>();

            // Remove create block
            transactions.drain(0..1);

            let cand_citizen = Citizen::new(*candidate.pub_key(),
                                            String::from(candidate.name()));
            let mut voters : Citizens = Vec::new();

            for transaction in transactions {
                match transaction {
                    ElectionTransactions::CreateVote(v) => voters.push(
                        Citizen::new (
                            *v.from(),
                            String::from(v.name()).clone(),
                        )
                    ),
                    _ => (),
                }
            }
            result.push(ElectionResultItem::new(cand_citizen,
                                             voters.len(),
                                             voters));
        }
        result.sort_by_key(|val| val.votes_count);
        result.reverse();
        Ok(ElectionResult::new(result))
    }
    pub fn candidate_info(state: &ServiceApiState, query: CandidateQuery) -> api::Result<CandidateInfo> {
        let snapshot = state.snapshot();
        let general_schema = blockchain::Schema::new(&snapshot);
        let elections_schema = Schema::new(&snapshot);

        match elections_schema.candidates().get(&query.pub_key) {
            Some(_) => {
                let max_height = general_schema.block_hashes_by_height().len() - 1;

                let block_proof = general_schema
                    .block_and_precommits(Height(max_height))
                    .unwrap();

                let to_table: MapProof<Hash, Hash> =
                    general_schema.get_proof_to_service_table(ELECTIONS_SERVICE_ID, 0);

                let to_candidate: MapProof<PublicKey, Candidate> =
                    elections_schema.candidates().get_proof(query.pub_key);

                let candidate_proof = CandidateProof {
                    to_table,
                    to_candidate,
                };

                let history = elections_schema.candidate_history(&query.pub_key);
                let proof = history.get_range_proof(0, history.len());

                let transactions: Vec<ElectionTransactions> = history
                    .iter()
                    .map(|record| general_schema.transactions().get(&record).unwrap())
                    .map(|raw| ElectionTransactions::tx_from_raw(raw).unwrap())
                    .collect::<Vec<_>>();

                let candidate_history = CandidateHistory {
                    proof,
                    transactions,
                };

                Ok(CandidateInfo {
                    block_proof,
                    candidate_proof,
                    candidate_history,
                })
            },
            _ => Err(Error::CandidateNotFound)?
        }
    }

    pub fn post_transaction(
        state: &ServiceApiState,
        query: ElectionTransactions,
    ) -> api::Result<TransactionResponse> {
        let transaction: Box<dyn Transaction> = query.into();
        let tx_hash = transaction.hash();
        state.sender().send(transaction)?;
        Ok(TransactionResponse { tx_hash })
    }

    pub fn wire(builder: &mut ServiceApiBuilder) {
        builder
            .public_scope()
            .endpoint_mut("v1/vote", Self::post_transaction)
            .endpoint_mut("v1/candidate/new", Self::post_transaction)
            .endpoint("v1/result", Self::result)
            .endpoint("v1/candidate/info", Self::candidate_info);
    }
}
