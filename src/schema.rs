use exonum::{
    crypto::{Hash, PublicKey}, storage::{Fork, ProofListIndex, ProofMapIndex, Snapshot},
};

use structs::Candidate;
use structs::Vote;

#[derive(Debug)]
pub struct Schema<T> {
    view: T,
}

impl<T> AsMut<T> for Schema<T> {
    fn as_mut(&mut self) -> &mut T {
        &mut self.view
    }
}

impl<T> Schema<T>
where
    T: AsRef<dyn Snapshot>,
{
    pub fn new(view: T) -> Self {
        Schema { view }
    }

    pub fn candidates(&self) -> ProofMapIndex<&T, PublicKey, Candidate> {
        ProofMapIndex::new("elections.candidates", &self.view)
    }

    pub fn votes(&self) -> ProofMapIndex<&T, PublicKey, Vote> {
        ProofMapIndex::new("elections.votes", &self.view)
    }

    pub fn candidate_history(&self, public_key: &PublicKey) -> ProofListIndex<&T, Hash> {
        ProofListIndex::new_in_family("elections.candidate_history", public_key, &self.view)
    }

    pub fn candidate(&self, pub_key: &PublicKey) -> Option<Candidate> {
        self.candidates().get(pub_key)
    }

    pub fn vote(&self, pub_key: &PublicKey) -> Option<Vote> {
        self.votes().get(pub_key)
    }

    pub fn state_hash(&self) -> Vec<Hash> {
        vec![self.candidates().merkle_root()]
    }
}

impl<'a> Schema<&'a mut Fork> {
    pub fn candidates_mut(&mut self) -> ProofMapIndex<&mut Fork, PublicKey, Candidate> {
        ProofMapIndex::new("elections.candidates", &mut self.view)
    }

    pub fn votes_mut(&mut self) -> ProofMapIndex<&mut Fork, PublicKey, Vote> {
        ProofMapIndex::new("elections.votes", &mut self.view)
    }

    pub fn candidate_history_mut(
        &mut self,
        public_key: &PublicKey,
    ) -> ProofListIndex<&mut Fork, Hash> {
        ProofListIndex::new_in_family("elections.candidate_history", public_key, &mut self.view)
    }

    pub fn add_vote(&mut self, vote: Vote, candidate: Candidate, transaction: &Hash) {
        self.votes_mut().put(vote.from(), vote.clone());
        let key = vote.to();
        let candidate = {
            let mut history = self.candidate_history_mut(key);
            history.push(*transaction);
            let history_hash = history.merkle_root();
            candidate.increase_votes_count(&history_hash)
        };
        self.candidates_mut().put(key, candidate.clone());
    }

    pub fn create_candidate(&mut self, key: &PublicKey, name: &str, transaction: &Hash) {
        let candidate = {
            let mut history = self.candidate_history_mut(key);
            history.push(*transaction);
            let history_hash = history.merkle_root();
            Candidate::new(key, name, 0, &history_hash)
        };
        self.candidates_mut().put(key, candidate);
    }
}
