#![allow(bare_trait_objects)]

use exonum::blockchain::ExecutionError;
use exonum::api;

#[derive(Debug, Fail)]
#[repr(u8)]
pub enum Error {
    #[fail(display = "Candidate has already been created")]
    CandidateAlreadyExists = 0,

    #[fail(display = "Citizen has already voted")]
    CitizenAlreadyVoted = 1,

    #[fail(display = "Candidate doesn't exist")]
    CandidateNotFound = 2,
}

impl From<Error> for ExecutionError {
    fn from(value: Error) -> ExecutionError {
        let description = format!("{}", value);
        ExecutionError::with_description(value as u8, description)
    }
}

impl From<Error> for api::Error {
    fn from(value: Error) -> api::Error {
        let description = format!("{}", value);
        api::Error::BadRequest(description)
    }
}