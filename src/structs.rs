use exonum::crypto::{Hash, PublicKey};

#[derive(Debug, Serialize, Deserialize, Clone, Copy)]
pub struct EmptyArg {
    arg: u32
}

impl EmptyArg {
    pub fn new(_res: u32) -> EmptyArg {
        EmptyArg {
            arg: _res,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Citizen {
    pub id: PublicKey,
    pub name: String,
}

impl Citizen {
    pub fn new(_id: PublicKey, _name: String) -> Citizen {
        Citizen {
            id: _id,
            name: _name,
        }
    }
}

pub type Citizens = Vec<Citizen>;

#[derive(Debug, Serialize, Deserialize)]
pub struct ElectionResultItem {
    pub candidate: Citizen,
    pub votes_count: usize,
    pub voters: Citizens,
}

impl ElectionResultItem {
    pub fn new(_candidate: Citizen,
               _votes_count: usize,
               _voters: Vec<Citizen>) -> ElectionResultItem {
        ElectionResultItem {
            candidate: _candidate,
            votes_count: _votes_count,
            voters: _voters,
        }
    }
}

pub type ElectionResultItems = Vec<ElectionResultItem>;

#[derive(Debug, Serialize, Deserialize)]
pub struct ElectionResult {
    pub result: ElectionResultItems,
}

impl ElectionResult {
    pub fn new(_result: ElectionResultItems) -> ElectionResult {
        ElectionResult {
            result: _result
        }
    }
}

encoding_struct! {
    struct Vote {
        from: &PublicKey,
        name: &str,
        to: &PublicKey,
    }
}

encoding_struct! {
    struct Candidate {
        pub_key: &PublicKey,
        name: &str,
        votes_count: u64,
        history_hash: &Hash,
    }
}

impl Candidate {
    pub fn increase_votes_count(self, history_hash: &Hash) -> Self {
        Self::new(
            self.pub_key(),
            self.name(),
            self.votes_count() + 1,
            history_hash,
        )
    }
}
