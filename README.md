# Exonum elections

This project implement a simple elections service using
[exonum](https://github.com/exonum/exonum) framework. All data 
is stored on the blockchain including participants names.

## Build project

```
git clone git@gitlab.com:justefg/exonum-elections.git
cargo build
```

## Run tests

Tests can be invoked by running

`cargo test`

## Run a service

`cargo run --example demo` by will listen to port `8000`


## Http endpoints

### Create a candidate using signed data in `POST` query
`api/services/elections/v1/candidate/create`

Input parameters:  
```
struct CreateCandidate {
    pub_key: &PublicKey,
    name: &str,
}
```
Output example:
```
{"tx_hash":"190a24ef72d16edd4d3a7af012f9f8c47203090ea6091e17114cec8a7dcc0968"}
```
### Vote for a candidate using signed transaction data in POST query
`api/services/elections/v1/vote`

Input parameters:
```
struct CreateVote {
    from: &PublicKey,
    name: &str,
    to: &PublicKey,
    // seed: u64,
}
``` 

### Proof for canditate x 
  `api/services/elections/v1/candidate/info`

  Input parameters:
```
  pub struct CandidateQuery {
    pub pub_key: PublicKey,
}
```
  Output example:
  ```
  {
   "candidate_proof" : {
      "to_candidate" : {
         "entries" : [
            {
               "key" : "34f1d5c627c28f2ab0f31f59602d6f7c3b311cce46cd1c982c5186c86f73a29b",
               "value" : {
                  "votes_count" : "0",
                  "name" : "Alice",
                  "history_hash" : "ad65c54e21bdb1e636d85c4d9239e244025b6be1ce675b0ac98901b239677dab",
                  "pub_key" : "34f1d5c627c28f2ab0f31f59602d6f7c3b311cce46cd1c982c5186c86f73a29b"
               }
            }
         ],
         "proof" : []
      },
      "to_table" : {
         "entries" : [
            {
               "key" : "50c8ba3a6170f0a2fb6736ece8a603576ef6309a35e810911599bc6211b554a9",
               "value" : "cd25e3469e9f33bc3cde6397725b7a6a31cfe22afe635cd2c6e566ec0ead95d4"
            }
         ],
         "proof" : [
            {
               "hash" : "46540a27bb14ef5f0d3863bf76d3f59f9682344547e0f95d93487a4c140fed17",
               "path" : "11"
            }
         ]
      }
   },
   "candidate_history" : {
      "proof" : {
         "val" : "ad65c54e21bdb1e636d85c4d9239e244025b6be1ce675b0ac98901b239677dab"
      },
      "transactions" : [
         {
            "service_id" : 128,
            "signature" : "5ba68fdaffaf69ef48b11b157a6c6c6a612708e1e43ef4ad23bf55680ccb39ee91df3c1889c8d0edd309787d4e212a62e2e94a7a8d8b58ddc2bbcc3e2ce7300d",
            "protocol_version" : 0,
            "message_id" : 1,
            "body" : {
               "pub_key" : "34f1d5c627c28f2ab0f31f59602d6f7c3b311cce46cd1c982c5186c86f73a29b",
               "name" : "Alice"
            }
         }
      ]
   },
   "block_proof" : {
      "block" : {
         "proposer_id" : 0,
         "height" : "1",
         "tx_count" : 1,
         "state_hash" : "d8a094404a96c66fbb8e767e55a8245ef2a395684005a32e0b994acb314b6425",
         "tx_hash" : "ad65c54e21bdb1e636d85c4d9239e244025b6be1ce675b0ac98901b239677dab",
         "prev_hash" : "fcd4ddbee01a0d7ad483256997708cbdce4e7a23cf21bc0c4cf739b6f3053d10"
      },
      "precommits" : [
         {
            "body" : {
               "validator" : 0,
               "round" : 1,
               "block_hash" : "f10b68c9842d769ac17aa40a0e0289fa0eb7a4d11fd062f33477d9ebd006d962",
               "height" : "1",
               "propose_hash" : "720da69900b516a1e19234f0b7121c45163a5d1333ef0794c965d4eb3c0b0c54",
               "time" : {
                  "nanos" : 597818000,
                  "secs" : "1535315721"
               }
            },
            "protocol_version" : 0,
            "signature" : "c21107deb8630e3066c51a2a0b20dbc95e9f9afd61d17574f26819ba3e31b5c1b280fa15ef5e625d507c1349b3cff97d269010dcf529a9c4038a37721e3c4a0d",
            "service_id" : 0,
            "message_id" : 4
         }
      ]
   }
}
```

### Get final elections result (entries are returned by **descending** votes_count)
  `api/services/elections/v1/candidate/result`


Output example:
```
{
   "result" : [
      {
         "candidate" : {
            "id" : "8f062c116529a558366768f28fd67390ea42120c1c89be41e272458850544bc4",
            "name" : "Alice"
         },
         "votes_count" : 2,
         "voters" : [
            {
               "id" : "c9684027ce7d0fe93f6f2f91a003fbc8f6a994d4eb2958fcc945f54747e277d8",
               "name" : "Alex"
            },
            {
               "id" : "928dbfd027d7bb9fffd94437df716bd28fc368c2c93a34a7bb8a0776ae20a0f5",
               "name" : "Anton"
            },
         ]
      },
      {
         "candidate" : {
            "name" : "Bob",
            "id" : "d4535b04bb5848fb6c82ff0c52299319867ec016eee1e9cf2542d10a6e334bf3"
         },
         "votes_count" : 1,
         "voters" : [
            {
               "id" : "9621299ff4c3aea9c65011987dd2d46dbfb027f44ef379e174b33cbe079de373",
               "name" : "Beth"
            }
         ]
      }
   ]
}

```